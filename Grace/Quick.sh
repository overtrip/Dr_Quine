#!/bin/bash
#Quick compile to check if all is ok

ex=( [1]="Result_Grace")

for var in ${ex[@]}
do
	if [ -f $var ]
	then
		rm -rf $var*
		echo "Delete tests files" $var*
	fi
done

if [ -f srcs/Grace.c ]
then
	echo "Compilation of Grace"
	./Grace
	diff -s Grace_kid.c srcs/Grace.c
fi
