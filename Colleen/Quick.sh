#!/bin/bash
#Quick compile to check if all is ok

ex=( [1]="Result_Colleen" )

for var in ${ex[@]}
do
	if [ -f $var ]
	then
		rm -rf $var*
		echo "Delete tests files" $var*
	fi
done

if [ -f srcs/Colleen.c ]
then
	echo "./Colleen > Result_Colleen"
	./Colleen > Result_Colleen
	echo "diff between Result_Colleen and Colleen.c"
	diff -s Result_Colleen srcs/Colleen.c
fi
